<div>
    <h2 class="flex justify-center w-full p-2 m-2 mx-auto text-white rounded shadow bg-emerald-900 shadow-inherit">
        {{Str::ucfirst(__('created user'))}}
    </h2>

    <form wire:submit.prevent="regedit" class=" w-full p-3 text-xl bg-[#3d3232] shadow  shadow-slate-900 rounded-tl-lg rounded-br-lg text-white">
        <div class="flex flex-col lg:flex-row lg:space-x-3">
            <label class="block w-full">
                <span class="mt-4 ">{{Str::ucfirst(__('name'))}}</span>
                <input wire:model.defer="user.name" type="text"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " autofocus placeholder="name" />
                @error("user.name")
                    <span class="text-red-300">{{ $message }}</span>
                @enderror
            </label>
            <label class="block w-full">
                <span class="mt-4 ">{{Str::ucfirst(__('username'))}}</span>
                <input wire:model.defer="user.username" type="text"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " placeholder="usuario" />
                @error("user.username")
                    <span class="text-red-300">{{ $message }}</span>
                @enderror
            </label>
        </div>
        <div class="flex flex-col lg:flex-row lg:space-x-3">
            <label class="block w-full">
                <span class="mt-4 ">{{Str::ucfirst(__('email'))}}</span>
                <input wire:model.defer="user.email" type="email"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3  placeholder:text-red-400 h-[3.95rem] " placeholder="correo" />
                @error("user.email")
                    <span class="text-red-300">{{ $message }}</span>
                @enderror
            </label>
            <label class="block w-full">
                <span class="mt-4 ">{{Str::ucfirst(__('bio'))}}</span>
                <textarea  rows="1" wire:model.defer="user.bio" class="mt-0 block w-full px-0.5 border-0 h-[3.95rem] border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400 " placeholder="biografía"></textarea>
                @error("user.bio")
                    <span class="text-red-300">{{ $message }}</span>
                @enderror
            </label>
        </div>

        {{-- @include('inc.panel.img') --}}
        <label class="relative flex items-center justify-center w-auto p-3 mx-auto my-5 border border-dashed rounded-tl-lg rounded-br-lg shadow h-96 shadow-inherit bg-slate-700">
            @if ($photo instanceof Livewire\TemporaryUploadedFile)
                <span wire:click="$set('photo')" class="absolute p-3 bg-orange-600 rounded-tl-lg rounded-br-lg shadow cursor-pointer bottom-2 right-2 shadow-inherit">
                    {{Str::ucfirst(__('change photo'))}}
                </span>
                <img class="w-full h-full bg-cover " src="{{ $photo->temporaryUrl() }}" alt="">
            @elseif ($user->image()->exists())
                <img src="{{ Storage::url(optional($user->image()->first())->url) }}" alt=""  class="h-full bg-cover ">
                <span class="absolute p-3 bg-orange-600 rounded-tl-lg rounded-br-lg shadow cursor-pointer bottom-2 right-2 shadow-inherit">
                {{Str::ucfirst(__('change photo'))}}
                </span>
                <input wire:model="photo" class="my-5 text-xl text-white border border-gray-300 cursor-pointer sr-only bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" type="file">
                @error("photo")
                <span class="absolute p-2 text-xl text-white top-2 bg-slate-950">{{ $message }}</span>
                @enderror
            @else
                <span class="p-3 bg-orange-600 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-inherit">
                {{Str::ucfirst(__('select profile picture'))}}
                </span>
                <input wire:model="photo" class="my-5 text-xl text-white border border-gray-300 cursor-pointer sr-only bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" type="file">
                @error("photo")
                <span class="absolute p-2 text-xl text-white top-2 bg-slate-950">{{ $message }}</span>
                @enderror
            @endif

        </label>


        <label class="block mt-4">
            <span class="">{{Str::ucfirst(__('password'))  }}</span>
            <input wire:model.defer="password" type="password" class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-xl italic mb-3 placeholder:text-red-400" />
            @error('password')
                <span class="text-red-300 ">{{ $message }}</span>
            @enderror
        </label>
        <div class="flex space-x-3 ">
            <input type="submit" class="block w-full p-3 mt-4 italic text-yellow-300 capitalize rounded-tl-lg rounded-br-lg shadow cursor-pointer bg-indigo-950 shadow-inherit " value="{{Str::ucfirst(__('create or edit user'))}}" />
            <a href="{{ route('usuarios.index') }}" class="flex justify-center w-full p-3 mt-4 italic text-yellow-300 capitalize rounded-tl-lg rounded-br-lg shadow cursor-pointer bg-indigo-950 shadow-inherit ">
                Cancelar
            </a>
        </div>
    </form>
</div>
