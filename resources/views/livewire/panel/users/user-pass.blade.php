<div>
    <form wire:submit.prevent="changepass" class="w-full p-3 mx-auto text-xl text-white rounded-tl-lg rounded-br-lg shadow shadow-slate-900 ">
        <label class="w-full">
            <span class="">{{Str::ucfirst(__('password'))}} actual</span>
            <input wire:model.defer="current_password" type="password"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-white " autofocus placeholder="***********" />
            @error("current_password")
                <span class="text-red-300">{{ $message }}</span>   
            @enderror
        </label>
        <div class="flex flex-col justify-center lg:flex-row lg:space-x-3">
            <label class="w-full">
                <span class="">Nueva {{Str::ucfirst(__('password'))}}</span>
                <input wire:model.defer="new_password" type="password"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-white " placeholder="********" />
                @error("new_password")
                    <span class="text-red-300">{{ $message }}</span>   
                @enderror
            </label>
            <label class="w-full">
                <span class="">Repetir {{Str::ucfirst(__('password'))}}</span>
                <input wire:model.defer="confirm_password" type="password"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-white " placeholder="************" />
                @error("confirm_password")
                    <span class="text-red-300 ">{{ $message }}</span>   
                @enderror
            </label>
        </div>
        <div class="flex items-center justify-center mt-3 space-x-3 ">
            <input type="submit" class="block w-full p-3 italic text-black bg-green-300 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-green-800 " value="{{__('Changed password')}}" />
            <a href="{{ route('user.index') }}"  class="flex justify-center w-full p-3 italic text-black bg-green-300 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-green-800" >
                {{__('Cancelar')}}
            </a>
        </div>
    </form>
</div>
