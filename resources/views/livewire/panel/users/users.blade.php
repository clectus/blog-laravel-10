<div>
    <div class="flex justify-center gap-2 p-1 m-2 bg-orange-800 rounded-md shadow shadow-current">
        <a class="block w-2/5 p-1 my-3 italic text-center text-white bg-pink-900 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-current" href="{{ route('usuario.create') }}">
            {{Str::ucfirst(__('created user'))}}
        </a>
        <input type="search" class=" text-white mt-0 w-full  px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-xl italic mb-3 placeholder:text-white" wire:model="search"  placeholder="Buscar ... ">
    </div>
    @if (session('msg'))
        <div class="flex justify-center p-2 m-2 text-white shadow bg-cyan-700 shadow-inherit">
            {{ Str::ucfirst(session('msg')) }}
        </div>
    @endif
    @forelse ($users as $user)
        <div class="flex flex-col p-3 mt-4 divide-y shadow shadow-black bg-zinc-900 ">
            <div class="flex flex-col items-center justify-center p-2 lg:flex-row lg:justify-start lg:space-x-3">
                <span class="text-xl text-white">Nombre(s) :</span>
                <li class="text-yellow-300 list-none ">
                    {{ Str::ucfirst(__($user->name))  }}
                </li>
            </div>
            <div class="flex flex-col items-center justify-center p-2 lg:flex-row lg:justify-start lg:space-x-3">
                <span class="text-xl text-white">Usuario :</span>
                <li class="text-yellow-300 list-none ">
                    @if ($user->username)
                        {{ __($user->username) }}
                    @else
                    <p class="flex justify-center text-white ">
                        {{ Str::ucfirst(__('no tiene url '))}}
                        {{ __('whoops!') }}
                    </p>
                    @endif
                </li>
            </div>
            <div class="flex flex-col items-center justify-center p-2 lg:flex-row lg:justify-start lg:space-x-3">
                <span class="text-xl text-white">Url amigable :</span>
                <li class="text-yellow-300 list-none ">
                    @if ($user->slug)
                        {{ Str::lower(__($user->slug))}}
                    @else
                    <p class="flex justify-center text-white ">
                        {{ Str::ucfirst(__('no tiene url '))}}
                        {{ __('whoops!') }}
                    </p>
                    @endif
                </li>
            </div>
            <div class="flex flex-col items-center justify-center p-2 lg:flex-row lg:justify-start lg:space-x-3">
                <span class="text-xl text-white">Imagen :</span>
                <li class="text-yellow-300 list-none ">
                    @if ($user->image)
                        <img src="{{ Storage::url($user->image()->first()->url) }}" alt=""  class="w-8 h-8 rounded-full ">
                    @else
                    <p class="flex justify-center text-white ">
                        {{ Str::ucfirst(__('no tiene imagen '))}}
                        {{ __('whoops!') }}
                    </p>
                    @endif
                </li>
            </div>
            <div class="flex flex-col items-center justify-center p-2 lg:flex-row lg:justify-start lg:space-x-3">
                <span class="text-xl text-white">Correo electrónico :</span>
                <li class="text-yellow-300 list-none ">
                    <a class="hover:underline" href="mailto:{{ __($user->email) }}">{{ __($user->email) }}</a>
                </li>
            </div>
            <div class="flex flex-col items-center justify-center p-2 lg:flex-row lg:justify-start lg:space-x-3">
                <span class="text-xl text-white">Biografia :</span>
                <li class="text-yellow-300 list-none ">
                    @if ($user->bio)
                        {{ Str::ucfirst($user->bio) }}
                    @else
                    <p class="flex justify-center text-white ">
                        {{ Str::ucfirst(__('no tiene biografia '))}}
                        {{ __('whoops!') }}
                    </p>
                    @endif
                </li>
            </div>
            <div class="flex justify-center p-3 space-x-3 text-yellow-300 lg:justify-start">

                <a href="{{ route('usuario.editar', $user) }}" class="p-2 rounded-tl-lg rounded-br-lg shadow shadow-black bg-rose-950" >
                    {{ Str::ucfirst(__('edit'))}}
                </a>
                <a href="" class="p-2 rounded-tl-lg rounded-br-lg shadow shadow-black bg-rose-950" >
                    {{ Str::ucfirst(__('delete'))}}
                </a>
            </div>
        </div>
    @empty
        <p class="flex justify-center p-2 m-2 text-white bg-black shadow shadow-inherit">

            {{ Str::ucfirst(__('no users registered.'))}}
            {{ __('whoops!') }}
        </p>
    @endforelse
</div>
