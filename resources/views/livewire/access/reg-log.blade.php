<div>
    {{-- bg-black text-yellow-300 p-4 shadow shadow-black rounded-bl-lg rounded-tr-lg md:w-[60%] lg:w-[50%] mx-auto text-xl --}}
    @if ($form)
        <form wire:submit.prevent="register" class=" md:w-[60%] lg:w-[40%] mx-auto text-white text-xl italic ">
            <fieldset class="p-3 bg-black border shadow shadow-black">
                <legend class="p-3 mx-auto text-2xl text-yellow-300 ">
                    Formulario de registro
                </legend>
                <label class="block w-full">
                    <span class="mt-4 ">{{Str::ucfirst(__('name'))}}(s) :</span>
                    <input wire:model.defer="name" type="text"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-green-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " autofocus placeholder="nombre(s)" />
                    @error("name")
                        <span class="text-red-300">{{ $message }}</span>
                    @enderror
                </label>
                <label class="block w-full">
                    <span class="mt-4 ">{{Str::ucfirst(__('username'))}} :</span>
                    <input wire:model.defer="username" type="text"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-green-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " autofocus placeholder="nombre de usuario" />
                    @error("username")
                        <span class="text-red-300">{{ $message }}</span>
                    @enderror
                </label>
                <label class="block w-full">
                    <span class="mt-4 ">{{Str::ucfirst(__('email'))}} :</span>
                    <input wire:model.defer="email" type="email"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " placeholder="correo electrónico" />
                    @error("email")
                        <span class="text-red-300">{{ $message }}</span>
                    @enderror
                </label>
                <label class="block w-full">
                    <span class="mt-4 ">{{Str::ucfirst(__('password'))}} :</span>
                    <input wire:model.defer="password" type="password"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " placeholder="contraseña" />
                    @error("password")
                        <span class="text-red-300">{{ $message }}</span>
                    @enderror
                </label>
                <label class="block w-full">
                    <span class="mt-4 ">{{Str::ucfirst(__('password_confirmation'))}} :</span>
                    <input wire:model.defer="password_confirmation" type="password"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " placeholder="contraseña" />
                    @error("password_confirmation")
                        <span class="text-red-300">{{ $message }}</span>
                    @enderror
                </label>
                <div class="flex items-center justify-center space-x-3 ">
                    <input type="submit" class="block w-full p-3 italic text-white bg-indigo-900 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-green-800 " value="{{ Str::ucfirst(__('register')) }}" />

                    <a class="flex justify-center w-full p-3 italic text-white bg-orange-800 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-green-800" wire:click.prevent="form">
                        {{ Str::ucfirst(__('login')) }}
                    </a>
                </div>
            </fieldset>
        </form>
    @else
        @if (session('msg'))
            <div class="flex justify-center p-2 mx-auto mb-4 text-xl text-white shadow w-96 shadow-black bg-slate-950">
                {{ Str::ucfirst(__(session('msg'))) }}
            </div>
        @endif
    <form wire:submit.prevent="login" class=" md:w-[60%] lg:w-[40%] mx-auto text-white text-xl italic ">
        <fieldset class="p-3 bg-black border shadow shadow-black">
            <legend class="p-3 mx-auto text-2xl text-yellow-300 ">
                {{ Str::ucfirst(__('login')) }}
            </legend>
            <label class="block w-full">
                <span class="mt-4 ">{{Str::ucfirst(__('username or email'))}} :</span>
                <input wire:model.defer="log_id" type="text"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400 " value="{{ Session::get('msgverify') ? Session::get('msgverify') : old('email') }}" placeholder="correo electrónico" />
                @error("log_id")
                    <span class="text-red-300">{{ $message }}</span>
                @enderror
            </label>
            {{-- <div class="my-3 ">
                <label class="inline-flex items-center">
                    <input wire:model.defer="remember" type="checkbox" class="border-transparent bg-blue-950 text-lime-700 ">
                    <span class="ml-2">Recordar sesión</span>
                </label>
              </div> --}}
            <label class="block w-full">
                <span class="mt-4 ">{{Str::ucfirst(__('password'))}} :</span>
                <input wire:model.defer="password" type="password"  class="mt-0 block w-full px-0.5 border-0 border-b-2 border-b-green-200 focus:ring-0 focus:border-yellow-300 appearance-none bg-none bg-transparent text-white text-xl italic mb-3 placeholder:text-red-400  " placeholder="contraseña" />
                @error("password")
                    <span class="text-red-300">{{ $message }}</span>
                @enderror
            </label>

            <div class="flex items-center justify-center mt-3 space-x-3 ">
                <input type="submit" class="block w-full p-3 italic text-white bg-indigo-900 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-green-800 " value="{{ Str::ucfirst(__('login')) }}" />

                <a class="flex justify-center w-full p-3 italic text-white bg-orange-800 rounded-tl-lg rounded-br-lg shadow cursor-pointer shadow-green-800" wire:click.prevent="form">
                    {{ Str::ucfirst(__('register')) }}
                </a>
            </div>
        </fieldset>
    </form>
    @endif
</div>
