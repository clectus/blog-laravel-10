<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? 'Runa' }}</title>
    <link rel="icon" href="{{ Vite::img('fig.jpg') }}" type="image/x-icon">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @livewireStyles
</head>
<body class="italic bg-[#144b42] ">
<main class="flex flex-col h-screen">
    <header class="relative top-0 w-full p-4 text-xl text-center text-orange-300 shadow shadow-slate-900 bg-stone-900">
        Runa
    </header>
    @include('inc.panel.nav-movil')
    <section class="grid flex-1 grid-cols-10 gap-2 p-3 overflow-y-scroll text-white ">
        <aside class="static hidden col-span-10 text-xl text-orange-300 lg:col-span-3 lg:grid">
            @include('inc.panel.nav-panel')
        </aside>
        <article class="col-span-10 p-3 text-xl bg-gray-800 rounded shadow lg:col-span-7 shadow-slate-950 ">
            {{ $slot }}
        </article>
    </section>
</main>
    @livewireScripts
</body>
</html>