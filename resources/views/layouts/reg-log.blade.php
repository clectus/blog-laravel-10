<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? 'Runa' }}</title>
    <link rel="icon" href="{{ Vite::img('fig.jpg') }}" type="image/x-icon">
    @livewireStyles
    @vite('resources/css/app.css')
</head>
<body class=" bg-slate-800 ">
    <main class=" m-3 ">
        {{ $slot }}
    </main>
    @livewireScripts
    @vite('resources/js/app.js')
</body>
</html>