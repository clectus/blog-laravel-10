<!-- Submenú Movil -->
<div x-data="{ open: false }">
    <!-- Button -->
    <button @click="open = ! open" class="absolute bottom-7 right-7 lg:hidden ">
        <svg class="w-7 h-7 text-white shadow shadow-slate-900 bg-black" fill="none" stroke="currentColor" stroke-width="1.5" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
            <path d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" stroke-linecap="round" stroke-linejoin="round">
            </path>
        </svg>
    </button>
    <div @click.outside="open = false" class="absolute top-16 bottom-7 left-0 flex flex-col w-4/5 h-screen text-white bg-black divide-y divide-red-300 shadow shadow-slate-900 lg:hidden" x-show="open">
        <a class="px-5 py-3 hover:bg-slate-700" href="#">
            About Us
        </a>
        <a class="px-5 py-3 hover:bg-slate-700" href="#">
            Contact Us
        </a>
        <a class="px-5 py-3 hover:bg-slate-700" href="#">
            Privacy Policy
        </a>
        <div class="">
            <button class="relative w-full px-5 py-2 text-white bg-green-600 peer hover:bg-green-700">
                Submenú
            </button>
            <!-- the menu here -->
            <div class="absolute flex-col hidden w-2/3 mt-1 peer-hover:flex hover:flex bg-slate-900 drop-shadow-lg">
                <a class="px-5 py-3 hover:bg-slate-700" href="#">
                    About Us
                </a>
                <a class="px-5 py-3 hover:bg-slate-700" href="#">
                    Contact Us
                </a>
                <a class="px-5 py-3 hover:bg-slate-700" href="#">
                    Privacy Policy
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Submenú Movil -->