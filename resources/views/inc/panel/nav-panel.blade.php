<nav class="bg-green-800 lg:flex flex-col p-2 rounded-tl-lg gap-1 rounded-br-lg shadow shadow-black ">
    @include('inc.panel.nav-perfil')
    <a class="shadow shadow-inherit bg-slate-950 hover:bg-stone-700 hover:text-white rounded-tl-lg rounded-br-lg p-2   " href="" >
        {{Str::ucfirst(__('categories'))}}
    </a>
    <a class="shadow shadow-inherit bg-slate-950 hover:bg-stone-700 hover:text-white rounded-tl-lg rounded-br-lg p-2 " href="" >
        {{ Str::ucfirst(__('articles'))  }}
    </a>
    <a class="shadow shadow-inherit bg-slate-950 hover:bg-stone-700 hover:text-white rounded-tl-lg rounded-br-lg p-2   " href="{{ route('usuarios.index') }}" >
        {{Str::ucfirst(__('users'))}}
    </a>
    <a class="shadow shadow-inherit bg-slate-950 hover:bg-stone-700 hover:text-white rounded-tl-lg rounded-br-lg p-2   " href="{{ route('panel.index') }}" >
        {{Str::ucfirst(__('panel'))}}
    </a>
</nav>