<?php

use App\Http\Livewire\Access\RegLog;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Panel\Users\Users;
use App\Http\Livewire\Panel\PanelControl;
use App\Http\Controllers\VerifyUserController;
use App\Http\Livewire\Panel\Users\UserRegiEdit;

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', PanelControl::class)->name('user');

Route::get('reg-log', RegLog::class)->name('login');
Route::post('reg-log', RegLog::class)->name('logout');

Route::get('account/verify/{token}', VerifyUserController::class)->name('verify');

Route::group(['middleware' => ['auth', 'user-veri-email'], 'prefix' => 'panel'], function(){

    Route::group(['prefix' => '', 'as' => 'panel.'], function(){

        // Panel
        Route::get('', PanelControl::class)->name('index');

    });

    Route::group(['prefix' => 'usuarios', 'as' => 'usuarios.'], function(){

        Route::get('', Users::class)->name('index');
        // Route::get('crear', UserForm::class)->name('create');
        // Route::get('{user}', UserShow::class)->name('show');

        // Route::get('{user}/borrar', Users::class)->name('delete');
        // Route::get('{user}/perfil', UserProfile::class)->name('details');
        // Route::view('/perfil/user', 'partials.panel.perfil')->name('profile');
    });

    Route::group(['prefix' => 'usuario', 'as' => 'usuario.'], function(){

        Route::get('crear', UserRegiEdit::class)->name('create');
        Route::get('{user}/editar', UserRegiEdit::class)->name('editar');

        // Route::get('{user}/borrar', Users::class)->name('delete');
        // Route::get('{user}/perfil', UserProfile::class)->name('details');
        // Route::view('/perfil/user', 'partials.panel.perfil')->name('profile');
    });
});
