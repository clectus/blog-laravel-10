<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Img extends Model
{
    use HasFactory;
    public $table = "imgs";

    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }
}
