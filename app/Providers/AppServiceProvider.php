<?php

namespace App\Providers;

use App\Models\Articulo;
use App\Models\User;
use Illuminate\Support\Facades\Vite;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Model::unguard();
        Vite::macro('img', fn (string $asset) => $this->asset("public/img/{$asset}"));
        Relation::enforceMorphMap([
            'Articulo' => Articulo::class,
            'Usuario' => User::class,
        ]);
    }
}
