<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait HasUpload
{
    public function uploadImage($model)
    {
        if ($model->image()->exists()) {
            $oldImage = $model->image()->where('imageable_id', $model->id)->first();
            Storage::disk('public')->delete([$oldImage->url]);
            $oldImage->delete();
        }
        $photoUrl = $this->photo->store('/', 'public');
        $model->image()->updateOrCreate(['url' => $photoUrl]);
    }
}
