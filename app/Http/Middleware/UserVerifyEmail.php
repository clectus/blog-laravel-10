<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserVerifyEmail
{
    public function handle(Request $request, Closure $next): Response
    {

        if (!Auth::user()->email_verified) {
            auth()->logout();
            return redirect()->route('login')
                    ->with('msg', Str::ucfirst(__('we have sent you a link, please check your email address')));
          }

        return $next($request);
    }
}
