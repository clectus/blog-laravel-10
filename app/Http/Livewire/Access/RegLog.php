<?php

namespace App\Http\Livewire\Access;

use Carbon\Carbon;
use App\Models\User;
use App\Models\UserVerify;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Session;

class RegLog extends Component {
    public $user, $email, $name, $token, $username, $log_id, $password, $password_confirmation, $remember;
    public $form = false;

    public function render(){
        return view('livewire.access.reg-log')
        ->layout('layouts.reg-log', ['title' => 'Registro inicio de sesion']);
    }

    public function form(){
        $this->form = !$this->form;
    }

    public function register(){
        $this->validate([
            'name'      => 'required|min:3',
            'username'  => 'required|min:3|alpha_dash|unique:users,username',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|min:3|confirmed:password_confirmation',
        ]);

        $user = User::create([
            'name'     => $this->name,
            'username' => $this->username,
            'email'    => $this->email,
            'password' => bcrypt($this->password),
        ]);

        $token   = $token = Str::random(64);
        $veriUrl = route('verify', ['token' => $token]);
        UserVerify::create([
            'user_id' => $user->id,
            'token'   => $token,
        ]);

         $msg  =  Str::ucfirst(__('dear')) . '<br/>' . Str::ucfirst('<b>'.$this->name.'</b>'). '<br/>';
         $msg .=  Str::ucfirst(__('thank you for registering on our site, now we just need you to verify your email to complete your registration and you can log in'));

         $mailData = [
             'recipiente' => $this->email,
             'para'       => $this->email,
             'nombre'     => $this->name,
             'sujeto'     => Str::ucfirst(__('email verification')),
             'cuerpo'     => $msg,
             'enlace'     => $veriUrl,
         ];

         Mail::send('mail',  $mailData, function($msg) use ($mailData){
             $msg->to(
                 $mailData['recipiente']
             )
             ->from(
                 $mailData['para'],
                 $mailData['nombre']
             )
             ->subject(
                 $mailData['sujeto']
             )
             ;
         });

        session()->flash('msg', Str::ucfirst(__('you have registered, now you need to verify your account, we have sent you an activation link, please check your email.')));
        $this->redirectRoute('login');
    }

    public function login(Request $request){
        $fieldtype = filter_var($this->log_id, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if ($fieldtype == 'email') {
            $this->validate([
                'log_id'    => 'required|email|exists:users,email',
                'password'  => 'required|min:3',
            ],[
                'log_id'            => Str::ucfirst(__('the email or username is required.')),
                'log_id.email'      => Str::ucfirst(__('invalid email.')),
                'log_id.exists'     => Str::ucfirst(__('email is not registered.')) ,
                'password.required' => Str::ucfirst( __('password required.'))
            ]);
        } else {
            $this->validate([
                'log_id'    => 'required|exists:users,username',
                'password'  => 'required|min:3',
            ],[
                'log_id.required'   =>Str::ucfirst(__('the email or username is required.')),
                'log_id.exists'     =>Str::ucfirst(__('username is not registered.')),
                'password.required' =>Str::ucfirst(__('password required.'))
            ]);
        }

        $creds = ([
            $fieldtype => $this->log_id,
            'password'  => $this->password,
        ]);

        if (Auth::guard('web')->attempt($creds)) {
            $checkuser = User::where($fieldtype, $this->log_id)->first();
            if ($checkuser->blocked == 1) {
                Auth::guard('web')->logout();
                session()->flash('msg', Str::ucfirst(__('your account has been blocked.')));
                $request->session()->regenerate();
                $this->redirectRoute('login');
            } else {
                session()->flash('msg', Str::ucfirst(__('you are Login successful.')));
                $this->redirectRoute('panel.index');
            }
        } else {
            session()->flash('msg', Str::ucfirst(__('incorrect username/email or password.')) );
        }


    }

    public function logout(Request $request){
        Session::flush();
        Auth::logout();
        $this->redirectRoute('login');
        session()->flash('msg', Str::ucfirst(__('session has been closed')));
    }
}
