<?php

namespace App\Http\Livewire\Panel;

use Livewire\Component;

class PanelControl extends Component
{
    public function render()
    {
        return view('livewire.panel.panel-control')
        ->layout('layouts.app-panel', ['title' => 'Control Panel']);
    }
}
