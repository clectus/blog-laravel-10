<?php

namespace App\Http\Livewire\Panel\Users;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserPass extends Component
{
    // public User $user;
    public $user, $current_password, $new_password, $confirm_password;


    // public function mount(User $user)
    // {
    //     $this->user = $user;    
    // }

    public function render()
    {
        return view('livewire.panel.users.user-pass')
        ->layout('layouts.app-panel', ['title' => 'Usuario contraseña']);
    }

    public function changepass()
    {
        $this->validate([
            'current_password'  => [
                'required',
                function ($value, $error)
                {
                    if (!Hash::check($value, User::find($this->user->id)->password)) {
                        return $error(__('the current password is incorrect'));
                    }
                },
            ],
            'new_password'     => 'required|min:3',
            'confirm_password' => 'same:new_password',
        
        ],[
            'current_password.required'  => __('enter your current password.'),
            'new_password.required'      => __('enter your new password.'),
            'confirm_password.same'      => __('confirme new password, must be equals to the new password'),
        ]);

        $query = User::find(auth('web')->id())->update([
            'password' => bcrypt($this->new_password)
        ]);

        if ($query) {
            session()->flash('msg', __('contra cambiada correctamente.'));
            $this->redirectRoute('usuarios.index');
            $this->current_password = $this->new_password = $this->confirm_password = null;
        } else {
            session()->flash('msg', __('error con las contraseñas.'));
        }
        
        // dd('los passworeds');
    }

}
