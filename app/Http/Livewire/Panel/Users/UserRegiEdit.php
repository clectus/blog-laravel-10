<?php

namespace App\Http\Livewire\Panel\Users;

use App\Models\Img;
use App\Models\User;
use App\Traits\hasUpload;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Validation\Rule;

class UserRegiEdit extends Component
{
    use WithFileUploads, hasUpload;

    public User $user;
    public $photo;
    public $password;

    protected function rules(){
        return [
            'user.name' => 'required|min:3|string',
            'user.username' => 'nullable|min:3',
            'user.slug' => [
                'nullable',
                'alpha_dash',
                Rule::unique('users', 'slug')->ignore($this->user)
            ],
            'user.email' => [
                'required',
                Rule::unique('users', 'email')->ignore($this->user)
            ],
            'user.bio' => 'nullable|min:3',
            'photo' => [
                $this->user->photo ? '' : '',
                'nullable',
                'image',
                'max:2048',
                // Rule::exists('users', 'photo')->ignore($this->user)
            ],
            // 'photo' => 'nullable|exists:users,photo,'.$this->user->photo,
            'password' => [
                'required',
                'min:3',
                Rule::unique('users', 'password')->ignore($this->user->id)
            ]
        ];
    }

    public function mount(User $user, Img $photo){
        $this->user = $user;
        $this->photo = $photo;
        $this->password = $user->password;
    }

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function regedit(){
        $this->validate();

        $this->user->password = bcrypt($this->password);
        $this->user->save();

        if ($this->photo) {
            $this->photo = $this->uploadImage($this->user);
        }

        session()->flash('msg', __('user successfully created or edited.'));
        $this->redirectRoute('usuarios.index');
    }

    public function render(){
        return view('livewire.panel.users.user-regi-edit')
            ->layout('layouts.app-panel', ['title' => 'Crear Usuario']);
    }
}
