<?php

namespace App\Http\Livewire\Panel\Users;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Users extends Component
{
    use WithPagination;

    public $search = '';

    public function render()
    {
        return view('livewire.panel.users.users', [
            'users' => User::where('name', 'like', "%{$this->search}%")->latest()->get()
        ])
        ->layout('layouts.app-panel', ['title' => 'Panel Usuarios']);
    }
}
